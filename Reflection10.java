package Reflection;

import java.lang.reflect.Method;

public class Reflection10 {

	public static void main(String[] args) throws Exception {
		ReflectionSimple obj = new ReflectionSimple();
		System.out.println("Reflection10");
		
		Method m = obj.getClass().getDeclaredMethod("setInput2", double.class);
		m.setAccessible(true);
		m.invoke(obj, 100);
		System.out.println(obj);
	}

}