package KUnitTest;

import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.*;

public class KUnitClass {
	  private static List<String> checks;
	  private static int checksMade = 0;
	  private static int passedChecks = 0;
	  private static int failedChecks = 0;

	  private static void addToReport(String txt) {
	    if (checks == null) {
	    	checks = new LinkedList<String>();
	    }
	    checks.add(String.format("%04d: %s", checksMade++, txt));
	  }
	  
	  public static void checkEquals(double value1, double value2) {
	    if (value1 == value2) {
	    	addToReport(String.format("  %.2f == %.2f", value1, value2));
	    	passedChecks++;
	    } else {
	    	addToReport(String.format("* %.2f == %.2f", value1, value2));
	    	failedChecks++;
	    }
	  }
	  
	  public static void checkStringCharAt(String value1, String value2) {
		try {
			char n1 = value1.charAt(4);
			char n2 = value2.charAt(4);
			if (value1.charAt(4) == n1 && value2.charAt(4) == n2) {
				addToReport(String.format("%s and %s more than 4 letters", value1, value2));
				passedChecks++;
		    } else {
		    	addToReport(String.format("%s and %s more than 4 letters", value1, value2));
		    	failedChecks++;
		    }
		}catch(StringIndexOutOfBoundsException e) {
			System.out.println("Inserted String must be more than 4 letters\n");
		}
		
	  }

	  public static void checkNotEquals(double value1, double value2) {
	    if (value1 != value2) {
	    	addToReport(String.format("  %.2f != %.2f", value1, value2));
	    	passedChecks++;
	    } else {
	    	addToReport(String.format("* %.2f != %.2f", value1, value2));
	    	failedChecks++;
	    }
	  }

	  public static void report() {
		  Logger logger = LogManager.getLogger(KUnitClass.class);
		  logger.info(passedChecks + " checks passed");
		  logger.warn(failedChecks + " checks falied");
		  
		  System.out.println();
		  System.out.printf("%d checks passed\n", passedChecks);
		  System.out.printf("%d checks failed\n", failedChecks);
		  System.out.println();
	    
		  for (String check : checks) {
			  logger.info(check);
	    }
	  }
}
