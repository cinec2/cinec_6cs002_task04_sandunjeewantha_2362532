package KUnitTestDemo;

import static KUnitTest.KUnitClass.*;

import java.lang.reflect.*;

public class UnitTestSampleClass {
	void checkConstructorAndAccess(){
		UnitTestingSample obj = new UnitTestingSample(50.2, 42.9);
	    checkEquals(obj.getInput1(), 50.0);
	    checkEquals(obj.getInput2(), 42.9);
	    checkNotEquals(obj.getInput2(), 50.1);    
	    checkNotEquals(obj.getInput2(), 42.9);    
	  }

	  void checkSqrtInput1(){
		  UnitTestingSample obj = new UnitTestingSample(50.2, 42.9);
		  obj.sqrtInput1();
		  checkEquals(obj.getInput1(), 42.9);
	  }
	  
	  void checkStringCharAtValues(){
		  UnitTestingSample obj = new UnitTestingSample(50.2, 42.9);
		  checkStringCharAt("Sandun", "Jeewantha");
		  obj = new UnitTestingSample(50.2, 0.0);
		  checkStringCharAt("Test", "k");
	  }
	  

	  public static void main(String[] args) throws Exception{
		  UnitTestSampleClass test = new UnitTestSampleClass();
		  test.checkStringCharAtValues();
		  test.checkConstructorAndAccess();
		  test.checkSqrtInput1();
	    
		  // Access to private variables
		  UnitTestingSample obj = new UnitTestingSample(10, 19);
		  Field fields = obj.getClass().getDeclaredField("input2");
		  fields.setAccessible(true);
		  checkEquals(fields.getDouble(obj), 19);
	    
		  report();
	  }
}
