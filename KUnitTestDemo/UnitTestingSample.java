package KUnitTestDemo;

public class UnitTestingSample {
	public double input1 = 50.5;
	private double input2 = 25.4;
	
	public UnitTestingSample() {
		
	}
	
	public UnitTestingSample(double input1, double input2) {
		this.input1 = input1;
		this.input2 = input2;
	}
	
	public void sqrtInput1() {
		this.input1 = Math.sqrt(this.input1);
	}
	
	private void sqrtInput2() {
		this.input2 = Math.sqrt(this.input2);
	}
	
	public double getInput1() {
		return input1;
	}
	
	private void setInput1(double input1) {
		this.input1 = input1;
	}
	
	public double getInput2() {
		return input2;
	}
	
	public void setInput2(double b) {
		this.input2 = b;
	}
	
	public String toString() {
		return String.format("Input1 : %.2f\nInput2 : %.2f", input1, input2);
	}
}
