package Reflection;

public class Reflection03 {

	public static void main(String[] args) {
		ReflectionSimple obj = new ReflectionSimple();
		System.out.println("Reflection03");
		System.out.println("class = " + obj.getClass());
		System.out.println("class name = " + obj.getClass().getName());
	}

}
