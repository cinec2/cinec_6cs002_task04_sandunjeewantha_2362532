package Reflection;

import java.lang.reflect.Field;

public class Reflection05 {

	public static void main(String[] args) throws Exception {
		ReflectionSimple obj = new ReflectionSimple();
		System.out.println("Reflection05");
		Field [] fields = obj.getClass().getDeclaredFields();
		System.out.printf("There are %d fields\n", fields.length);
		for(Field f : fields) {
			System.out.printf("field name = %s, type = %s, value = %.2f\n", f.getName(), f.getType(), f.getDouble(obj));
		}
	}

}